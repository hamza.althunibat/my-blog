---
title: "Preparing Development Environment"
date: 2021-06-21T22:32:11+04:00
draft: false
---
## Introduction

There are many articles and guidelines show how to prepare a cloud native development workspace on your PC or laptop, here I'm sharing mine, a little tweeks here and there that makes my daily life much easier and comfort.

Before we go into the details, let's put some requirements to fulfill first:

1. It is based on Kubernetes
2. The workspace should be per project.
3. I can easlly create, delete, and restore the workspace.
4. Support Load balancer services and dynamic IP allocation.
5. Optionally, if I have DNS, I can automatically create DNS entries for external Load balancers.

## What do I use

So, in order to fulfill the requirements, I have choosen the below weapons to be in my toolkit:

1. Ubuntu Desktop 20.04 LTS as my Operating System on my Dell Laptop.

2. [`zsh`](https://www.zsh.org/), "`[oh my zsh](https://ohmyz.sh/ "Unleash your terminal like never before")"`, `[Terminator](https://terminator-gtk3.readthedocs.io/en/latest/)`, `kubcectl`, `git`, and `jq`.

3. Docker distribution by Ubuntu/Debian "`[docker.io](https://packages.debian.org/sid/docker.io)`".

4. `[K3d](https://k3d.io/)` as handy cli to manage K3s clusters.

5. [l`ocal-path-provisioner`](https://github.com/rancher/local-path-provisioner) for dynamically provision storage

6. `[metallb](https://metallb.universe.tf/)` which provides a network load-balancer implementation.

7. [`Cloudflare`](https://www.cloudflare.com/) as DNS provider, got xyz domain for ~8 $

8. [`external-dns`](https://github.com/kubernetes-sigs/external-dns "Configure external DNS servers (AWS Route53, Google CloudDNS and others) for Kubernetes Ingresses and Services") as Kubernetes external DNS provider for load balancer services.

9. [`K9s`](https://k9scli.io/) as client for my Kubernetes.

## Workspace Preparation

I'll start from already having Ubuntu or any Linux distribution you like, a terminal of choice, with docker is already installed and working.

### Installing K3d

Referencing [github repository of K3d](https://github.com/rancher/k3d), lastest version can be installed automatically using the provided script `curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash` or `wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash`, once the it is installed, you can verify the installation by running `k3d --version`.

### Creating the first cluster

k3d is a lightweight wrapper to run [k3s](https://github.com/rancher/k3s) (Rancher Lab’s minimal Kubernetes distribution) in docker. the cluster I'm looking for has one manager node, and 3 worker nodes, and has a storage space i can monitor from my ubuntu machine.

```bash
k3d cluster create dev-cluster \                                                                                                                                                                                                ─╯
    --no-lb -a 3 \
    --k3s-server-arg '--disable=servicelb' --k3s-server-arg '--disable=traefik'\
    -v "$HOME/k3d_storage:/var/lib/rancher/k3s/storage@agent[0,1,2]"
```

By running above command, we have instructed k3d to do the following:

- Create cluster naming it "dev-cluster"
- Not to deploy load balancer node in front of K3s cluster.
- Disable packaged components with k3s:
  - servicelb: loadbalancer implementation, I'll replace it with matallb for better customization and control.
  - traefik ingress, it is an ingress provider, and at this moment I don't need it.
- Map local path `"$HOME/k3d_storage"` to be available on each agent node to be used by local-path-provisioner on path `/var/lib/rancher/k3s/storage`.

### Deploying metallb

Before we deploy metallb, we need to create a yaml file for configuration of IP range available to be dynamically allocated to load balancer services in K8s, and we can get the needed details by running the following command: `docker network inspect k3d-dev-cluster | jq -r 'map(.IPAM.Config[].Subnet)[]'`, in my case it returned `172.18.0.0/16`.

below yaml file configure metallb to utilize the  ip range `172.18.6.0-172.18.15.250` to be used by load balancer services.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 172.18.6.0-172.18.15.250
``````bash
#!/usr/bin/sh
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/metallb.yaml
kubectl apply -f ./metallb-config.yaml
```

### Deploying external-dns

external-dns supports many DNS providers, and for my case it is cloudflare. In order to deploy it, I uses the below yaml file:

`kubectl apply -f ./external_dns_config.yaml -n kube-system`

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: external-dns
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: external-dns
rules:
  - apiGroups: [""]
    resources: ["services", "endpoints", "pods"]
    verbs: ["get", "watch", "list"]
  - apiGroups: ["extensions", "networking.k8s.io"]
    resources: ["ingresses"]
    verbs: ["get", "watch", "list"]
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["list", "watch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: external-dns-viewer
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: external-dns
subjects:
  - kind: ServiceAccount
    name: external-dns
    namespace: kube-system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: external-dns
spec:
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: external-dns
  template:
    metadata:
      labels:
        app: external-dns
    spec:
      serviceAccountName: external-dns
      containers:
        - name: external-dns
          image: k8s.gcr.io/external-dns/external-dns:v0.8.0
          args:
            - --source=service # ingress is also possible
            - --domain-filter=godwit.xyz # (optional) limit to only example.com domains; change to match the zone created above.
            - --provider=cloudflare
          env:
            - name: CF_API_TOKEN
              value: "<provide API token here>"
            - name: CF_API_EMAIL
              value: "<provide your registered email here>"
          resources: {}
```

Note that the token supplied should be granted Zone `Read`, DNS `Edit` privileges, and access to `All zones`.

To test the configuration of both load balancer "metallb" and external dns provisioner "external-dns", I use the following nginx deployment

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
        ports:
        - containerPort: 80
        resources:
          {}
---
apiVersion: v1
kind: Service
metadata:
  name: nginx
  annotations:
    external-dns.alpha.kubernetes.io/hostname: nginx.example.com
    external-dns.alpha.kubernetes.io/ttl: "120" #optional
spec:
  selector:
    app: nginx
  type: LoadBalancer
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

external-dns is looking for annotations assigned into service metadata  `external-dns.alpha.kubernetes.io/hostname: example.com` and `external-dns.alpha.kubernetes.io/ttl: "120" #optional`

## What did we achieve?

- [x] Dynamically create K8s cluster that runs on top of docker, and utilize docker network stack.
- [x] Have storage dynamically provisioned within K8s on a managed location.
- [x] Dynamically assign IPs for load balancer services and access those IPs locally within your PC / laptop
- [x] Dynamically provision DNS for each load balancer service.

## ENJOY
